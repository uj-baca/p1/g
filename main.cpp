#include <iostream>
#include <string>
#include <fstream>

#define TEMPORARY "tmp.txt"

using namespace std;

fstream file;

void createFile(const string &filename, int amount) {
    string ident;
    int age;
    float height;
    file.open(filename.c_str(), fstream::out);
    file << amount << endl;
    for (int j = 0; j < amount; j++) {
        cin >> ident >> height >> age;
        file << ident << " " << height << " " << age << endl;
    }
    file.close();
}

void displayFile(const string &filename, int position, int amount) {
    int current = 0, age, x;
    file.open(filename.c_str(), fstream::in);
    file >> x;
    string ident;
    double height;
    while (x--) {
        file >> ident >> height >> age;
        if (current >= position && current < position + amount) {
            cout << current << ": " << ident << " " << height << " " << age << endl;
        }
        current++;
    }
    file.close();
    cout << " " << endl;
}

void insertToFile(const string &filename, int place, int amount) {
    string ident;
    float height;
    int age, records;
    fstream tmp;
    file.open(filename.c_str(), fstream::in);
    tmp.open(TEMPORARY, fstream::out);
    file >> records;
    if (place > records) {
        place = records;
    }
    records += amount;
    tmp << records << endl;
    for (int i = 0; i < records; ++i) {
        if (i < place) {
            file >> ident >> height >> age;
            tmp << ident << " " << height << " " << age << endl;
        } else if (amount > 0) {
            cin >> ident >> height >> age;
            tmp << ident << " " << height << " " << age << endl;
            amount--;
        } else {
            file >> ident >> height >> age;
            tmp << ident << " " << height << " " << age << endl;
        }
    }
    tmp.close();
    file.close();
    tmp.open(TEMPORARY, fstream::in);
    file.open(filename.c_str(), fstream::out);
    tmp >> records;
    file << records;
    while (!tmp.eof()) {
        tmp >> ident >> height >> age;
        if (!tmp.eof()) {
            file << endl << ident << " " << height << " " << age;
        }
    }
    tmp.close();
    file.close();
    remove(TEMPORARY);
}

void deleteFromFile(const string &filename, int place, int amount) {
    string tmpIdent;
    int tmpAge;
    float tmpHeight;
    fstream tmp;
    file.open(filename.c_str(), fstream::out | fstream::in);
    tmp.open(TEMPORARY, fstream::out | fstream::trunc);
    tmp << "";
    tmp.close();
    tmp.open(TEMPORARY, fstream::out | fstream::in);
    int x;
    file >> x;
    tmp << (((x - amount) < place) ? place : x - amount) << endl;
    for (int i = 0; i < x; i++) {
        file >> tmpIdent >> tmpHeight >> tmpAge;
        if (i < place || i >= place + amount) {
            tmp << tmpIdent << " " << tmpHeight << " " << tmpAge << endl;
        }
    }
    file.close();
    tmp.close();
    file.open(filename.c_str(), std::fstream::out | std::fstream::trunc);
    tmp.open(TEMPORARY, fstream::out | fstream::in);
    tmp >> x;
    file << x << endl;
    for (int i = 0; i < x; i++) {
        tmp >> tmpIdent >> tmpHeight >> tmpAge;
        file << tmpIdent << " " << tmpHeight << " " << tmpAge << endl;
    }
    file.close();
    tmp.close();
    remove(TEMPORARY);
}

void searchInFile(const string &filename, const string &searchIdent, float startHeight, float finishHeight, int searchAge) {
    int current = -1;
    string ident;
    float height;
    int age;
    file.open(filename.c_str(), fstream::in);
    while (true) {
        if (current < 0) {
            file >> age;
            current++;
        } else {
            file >> ident >> height >> age;
            if (file.eof()) {
                break;
            }
            if (searchIdent == ident || searchIdent == "*") {
                if (searchAge == age || searchAge == 0) {
                    if (startHeight <= height && finishHeight >= height) {
                        cout << current << ": " << ident << " " << height << " " << age << endl;
                    }
                }
            }
            current++;
        }
    }
    file.close();
    cout << " " << endl;
}

void sortFile(const string &filename, const string &output) {
    fstream in_file;
    fstream sorted_file;
    fstream temp_file;

    remove(output.c_str());

    in_file.open(filename.c_str(), fstream::in);

    string sorted_ident, ident;
    float sorted_height, height;
    int sorted_age, age;

    int records;
    int sorted_records = 0;

    in_file >> records;

    while (records > sorted_records) {
        in_file >> ident >> height >> age;

        sorted_file.open(output.c_str(), ios::in);
        temp_file.open(TEMPORARY, ios::out);

        sorted_file >> sorted_records;
        temp_file << sorted_records + 1 << endl;
        while (sorted_file.is_open() ? !sorted_file.eof() : !ident.empty()) {
            sorted_file >> sorted_ident >> sorted_height >> sorted_age;

            if (!ident.empty() && (sorted_ident.empty() || sorted_ident > ident)) {
                temp_file << ident << " " << height << " " << age << endl;
                ident = "";
            }

            if (!sorted_ident.empty()) {
                temp_file << sorted_ident << " " << sorted_height << " " << sorted_age << endl;
                sorted_ident = "";
            }
        }
        sorted_file.close();
        temp_file.close();

        sorted_file.open(output.c_str(), ios::out);
        temp_file.open(TEMPORARY, ios::in);

        temp_file >> sorted_records;
        sorted_file << sorted_records << endl;
        while (!temp_file.eof()) {
            temp_file >> sorted_ident >> sorted_height >> sorted_age;
            if (!temp_file.eof()) {
                sorted_file << sorted_ident << " " << sorted_height << " " << sorted_age << endl;
            }
        }

        sorted_file.close();
        temp_file.close();
    }


    remove(TEMPORARY);
    in_file.close();
    in_file.open(filename.c_str(), fstream::out);
    in_file << 0;
    in_file.close();
}

void mergeFiles(const string &prev, const string &next, const string &merge) {
    fstream filePrev, fileNext, fileMerge;
    filePrev.open(prev.c_str(), fstream::in);
    fileNext.open(next.c_str(), fstream::in);
    fileMerge.open(merge.c_str(), fstream::out);

    int records;
    string prevIdent, nextIdent;
    float prevHeight = 0;
    float nextHeight = 0;
    int prevAge;
    int nextAge = 0;

    filePrev >> records;
    fileNext >> prevAge;
    records += prevAge;

    fileMerge << records << endl;

    prevIdent = nextIdent = "";

    for (int i = 0; i < records; ++i) {
        if (!filePrev.eof() && prevIdent.empty()) {
            filePrev >> prevIdent >> prevHeight >> prevAge;
        }
        if (!fileNext.eof() && nextIdent.empty()) {
            fileNext >> nextIdent >> nextHeight >> nextAge;
        }

        bool writePrev;
        if (filePrev.eof() && prevIdent.empty()) {
            writePrev = false;
        } else if (fileNext.eof() && nextIdent.empty()) {
            writePrev = true;
        } else {
            writePrev = prevIdent <= nextIdent;
        }

        if (writePrev) {
            fileMerge << prevIdent << " " << prevHeight << " " << prevAge << endl;
            prevIdent = "";
        } else {
            fileMerge << nextIdent << " " << nextHeight << " " << nextAge << endl;
            nextIdent = "";
        }
    }

    filePrev.close();
    fileNext.close();
    fileMerge.close();
}

int main() {
    int count;
    string command, filename;
    cin >> count;
    for (int i = 0; i < count; i++) {
        cin >> command;
        if (command == "create") {
            int amount;
            cin >> filename >> amount;
            createFile(filename, amount);
        } else if (command == "display") {
            int position, amount;
            cin >> filename >> position >> amount;
            displayFile(filename, position, amount);
        } else if (command == "insert") {
            int place, amount;
            cin >> filename >> place >> amount;
            insertToFile(filename, place, amount);
        } else if (command == "delete") {
            int place, amount;
            cin >> filename >> place >> amount;
            deleteFromFile(filename, place, amount);
        } else if (command == "search") {
            string searchIdent;
            float startHeight, finishHeight;
            int searchAge;
            cin >> filename >> searchIdent >> searchAge >> startHeight >> finishHeight;
            searchInFile(filename, searchIdent, startHeight, finishHeight, searchAge);
        } else if (command == "sort") {
            string output;
            cin >> filename >> output;
            sortFile(filename, output);
        } else if (command == "merge") {
            string prev, next, merge;
            cin >> prev >> next >> merge;
            mergeFiles(prev, next, merge);
        }
    }
}
